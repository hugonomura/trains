all: test run clean

test:
	python -m unittest discover -v -s tests -p '*_test.py'

test_town:
	python -m unittest discover -v -s tests -p 'town_test.py'

test_graph:
	python -m unittest discover -v -s tests -p 'graph_test.py'

run:
	python trains.py input.txt

clean:
	rm -rf */__pycache__
	find -iname '*.pyc' -exec rm {} \;
