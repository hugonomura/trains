class Town:
    def __init__(self, label):
        self.label = label
        self.neighboring_towns = {}

    def add_neighboring_town(self, town, distance, time=None):
        self.neighboring_towns[town] = {'distance': distance, 
            'time': time}
