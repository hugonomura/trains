from trains.town import Town
from collections import defaultdict

class Graph():
    def __init__(self):
        self.nodes = {}

    def create_node(self, label):
        try:
            self.nodes[label]
            return False
        except KeyError:
            self.nodes[label] = Town(label)
            return True

    def add_edge(self, source, target, distance, time=None):
        self.create_node(source)
        self.create_node(target)
        self.nodes[source].add_neighboring_town(target, distance, time)

    def __breadth_first_search(self, source, target):
        known_paths = []
        queue = [(source, [source])]
        while queue:
            (vertex, path) = queue.pop(0)
            for next in set(self.nodes[vertex].neighboring_towns.keys()) - set(path[1:]):
                if next == target:
                    known_paths.append(path + [next])
                else:
                    queue.append((next, path + [next]))
        return known_paths

    def __breadth_first_search_with_cycle(self, source, target, max_stops=float('inf'), max_distance=float('inf'), max_time=float('inf')):
        known_paths = []
        queue = [(source, [source])]
        while queue:
            (vertex, path) = queue.pop(0)
            if len(path) <= max_stops and self.path(path) < max_distance and self.path(path, 'time') < max_time:
                for next in set(self.nodes[vertex].neighboring_towns.keys()):
                    if next == target and self.path(path + [next]) <= max_distance and self.path(path + [next], 'time') <= max_time:
                        known_paths.append(path + [next])
                        queue.append((next, path + [next]))
                    else:
                        queue.append((next, path + [next]))
        return known_paths

    def path(self, path, argument='distance'):
        total_cost = 0
        for index, node in enumerate(path):
            if not index + 1 == len(path):
                current_node = self.nodes[node]
                next_node = path[index + 1]
                try:
                    total_cost += current_node.neighboring_towns[next_node][argument]
                except (KeyError, TypeError) as e:
                    return None
        return total_cost

    def all_paths_between(self, source, target, **arguments):
        if not arguments:
            return self.__breadth_first_search(source, target)
        else:
            infinite = float('inf')
            arguments = defaultdict(lambda: infinite, arguments)
            max_distance = arguments['max_distance']
            max_time = arguments['max_time']
            max_stops = arguments['max_stops']
            if(max_stops == infinite and max_distance == infinite and max_time == infinite):
                raise ValueError('Invalid arguments use max_distance or max_stops or max_time keys')
            return self.__breadth_first_search_with_cycle(source, target, max_stops, max_distance, max_time)

    def all_paths_between_with_n_stops(self, source, target, n_stops):
        all_paths = self.__breadth_first_search_with_cycle(source, target, n_stops)
        return [path for path in all_paths if len(path) == n_stops + 1] # plus origin

    def shortest_time_path_between(self, source, target):
        paths = self.all_paths_between(source, target)
        if not paths:
            return paths
        return min(paths, key=lambda path: self.path(path, 'time'))

    def shortest_distance_path_between(self, source, target):
        paths = self.all_paths_between(source, target)
        if not paths:
            return paths
        return min(paths, key=self.path)

    def shortest_distance_between(self, source, target):
        paths = self.all_paths_between(source, target)
        if not paths:
            return None
        return self.path(min(paths, key=self.path))
