from  sys import argv
from trains.graph import Graph

def main(args):
    with open(str(args[1]), 'r') as f:
        edges = __parser_user_entry(f.read())
        graph = Graph()
        for edge in edges:
            source = edge[0]
            destiny = edge[1]
            distance_time = edge[2:].split('_')
            distance = int(distance_time[0])
            time = int(distance_time[1])
            graph.add_edge(source, destiny, distance, time)
        results = __queries(graph)
        for index, result in enumerate(results):
            if not result or result == 0:
                result = 'NO SUCH ROUTE'
            print("Output #{}: {}".format(index+1, str(result)))

def __queries(graph):
    list_of_queries = [
                        graph.path(['A', 'B', 'C']), # 1
                        graph.path(['A', 'D']), # 2
                        graph.path(['A', 'D', 'C']), # 3
                        graph.path(['A', 'E', 'B', 'C', 'D']), # 4
                        graph.path(['A', 'E', 'D']), # 5
                        len( # 6
                             graph.all_paths_between('C', 'C', max_stops=3)
                        ),
                        len( # 7
                            graph.all_paths_between_with_n_stops('A', 'C', 4)
                        ),
                        graph.shortest_distance_between('A', 'C'), # 8
                        graph.shortest_distance_between('B', 'B'), # 9
                        len( # 10
                            graph.all_paths_between('C', 'C', max_distance=29)
                        ),
                        graph.shortest_time_path_between('A', 'D')
                      ]
    return list_of_queries

def __parser_user_entry(edges):
    return [edge.strip() for edge in edges.split(',')]

if __name__ == '__main__':
    main(argv)
