# Trains
aplicação que dadas as rotas das ferrovias entre as cidades de Kiwiland, retorna o resultado dos caminhos solicitados conforme solicitado na descrição do problema, tomando como parâmetros:
- cidade origem / destino
- número máximo de paradas
- distância máxima do caminho
- número exato de paradas

## suposições
- cada cidade é identificada por apenas 1 caractere
- a distância entre as cidades é composta apenas por números inteiros
- não existem corner cases além dos usados no exemplo fornecido pelo vagas:
  - os caminhos passando por cidades já visitadas devem ser considerados, quando existem valores máximos para as distâncias / número máximo de paradas
- não existem 2 estradas saindo da mesma cidade em direção a uma mesma cidade diretamente
- as buscas levam em busca apenas cidades fazem parte do conjunto de entradas

## design
- a solução desse problema foi elaborada usando uma abordagem de grafos
- para a solução, foi usado em grafo direcionado, onde cada vértice é uma cidade e a distância entre uma cidade e outra é o valor da aresta
- cada cidade (objeto da classe Town) possui conhecimento apenas do seu nome e da lista de cidades vizinhas, bem como a distância para o caminho direto
- a classe Graph é responsável por descobrir os caminhos não diretos entre as cidades, sendo que a base para a descoberta dos caminhos foi o uso da busca em largura (BFS) e uma adaptação da busca em largura para considerar caminhos cíclicos.

## rodando a aplicação
como a aplicação foi feita apenas usando as funções nativas do Python 3 (no Makefile, a execução é feita usando a versão do Python default do sistema, geralmente a 2.7, pois o Python 3 não vem por default no Mac OS), não é necessária a instalação de nenhum pacote adicional para a execução da aplicação.
com o intuito de facilitar a execução dos comandos, foi criado um **Makefile** (o make vem por padrão em ambientes Unix) com as principais funções que serão executadas, conforme a lista abaixo:

### make test
executa todos os testes presentes na pasta __tests__

### make run
executa a aplicação, onde a entrada que sera usada para a execução da aplicação está presente no arquivo __input.txt__. a saída é exibida na saída default do terminal.

### make clean
remove todos os arquivos com a extensão ``pyc``, além de remover a pasta temporária ``__pycache__``, criados durante a execução da aplicação

### make all
executa todos os __testes__, a __aplicação__ e depois um __clean__ no projeto

