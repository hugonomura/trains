import unittest
import context
from trains.graph import Graph
from trains.town import Town

class TestGraphMethods(unittest.TestCase):
    def setUp(self):
        self.graph = Graph()

    def tearDown(self):
        self.graph = None

    def test_graph_without_any_node(self):
        expected_result = {}
        result = self.graph.nodes
        self.assertEqual(result, expected_result)

    def test_create_node_method_new_node(self):
        result = self.graph.create_node('A')
        self.assertTrue(result)

    def test_create_node_method_with_an_existent_node(self):
        self.graph.create_node('A')
        result = self.graph.create_node('A')
        self.assertFalse(result)

    def test_add_edge_method(self):
        town = 'A'
        other_town = 'B'
        distance_between_towns = 10
        self.graph.add_edge(town, other_town, distance_between_towns)
        result = self.graph.nodes[town]
        expected_result = town
        self.assertIsInstance(result, Town)

    def test_path_weight_method_existent_path(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        distance_between_A_B = 10
        distance_between_B_C = 10
        self.graph.add_edge(first_town, second_town, distance_between_A_B)
        self.graph.add_edge(second_town, third_town, distance_between_B_C)
        result = self.graph.path([first_town, second_town, third_town])
        expected_result = distance_between_A_B + distance_between_B_C
        self.assertEqual(result, expected_result)

    def test_path_time_method_existent_path(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        distance_between_A_B = 10
        distance_between_B_C = 10
        default_time = 20
        self.graph.add_edge(first_town, second_town, distance_between_A_B, default_time)
        self.graph.add_edge(second_town, third_town, distance_between_B_C, default_time)
        result = self.graph.path([first_town, second_town, third_town], 'time')
        expected_result = 2 * default_time
        self.assertEqual(result, expected_result)

    def test_path_weight_method_unexistent_path(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        distance_between_A_B = 10
        self.graph.add_edge(first_town, second_town, distance_between_A_B)
        self.graph.create_node(third_town)
        result = self.graph.path([first_town, second_town, third_town])
        self.assertEqual(result, None)

    def test_all_paths_between_method(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        another_town = 'D'
        default_distance = 10
        self.graph.add_edge(first_town, second_town, default_distance)
        self.graph.add_edge(second_town, third_town, default_distance)
        self.graph.add_edge(third_town, another_town, default_distance)
        self.graph.add_edge(second_town, another_town, default_distance)
        result = self.graph.all_paths_between(first_town, another_town)
        first_path = [first_town, second_town, another_town]
        second_path = [first_town, second_town, third_town, another_town]
        expected_result = [first_path, second_path]
        self.assertEqual(result, expected_result)

    def test_all_paths_between_method_with_max_stops(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        another_town = 'D'
        default_distance = 10
        self.graph.add_edge(first_town, second_town, default_distance)
        self.graph.add_edge(second_town, third_town, default_distance)
        self.graph.add_edge(third_town, another_town, default_distance)
        self.graph.add_edge(third_town, first_town, default_distance)
        result = self.graph.all_paths_between(first_town, another_town, max_stops=6)
        first_path = [first_town, second_town, third_town,another_town]
        second_path = [first_town, second_town, third_town, first_town, second_town, third_town,another_town]
        expected_result = [first_path, second_path]
        self.assertEqual(result, expected_result)

    def test_all_paths_between_method_with_max_distance(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        another_town = 'D'
        default_distance = 10
        self.graph.add_edge(first_town, second_town, default_distance)
        self.graph.add_edge(second_town, third_town, default_distance)
        self.graph.add_edge(third_town, another_town, default_distance)
        self.graph.add_edge(third_town, first_town, default_distance)
        result = self.graph.all_paths_between(first_town, another_town, max_distance=70)
        first_path = [first_town, second_town, third_town,another_town]
        second_path = [first_town, second_town, third_town, first_town, second_town, third_town,another_town]
        expected_result = [first_path, second_path]
        self.assertEqual(result, expected_result)

    def test_all_paths_between_method_with_max_time(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        another_town = 'D'
        default_distance = 10
        default_time = 10
        self.graph.add_edge(first_town, second_town, default_distance, default_time)
        self.graph.add_edge(second_town, third_town, default_distance, default_time)
        self.graph.add_edge(third_town, another_town, default_distance, default_time)
        self.graph.add_edge(third_town, first_town, default_distance, default_time)
        result = self.graph.all_paths_between(first_town, another_town, max_time=70)
        first_path = [first_town, second_town, third_town,another_town]
        second_path = [first_town, second_town, third_town, first_town, second_town, third_town,another_town]
        expected_result = [first_path, second_path]
        self.assertEqual(result, expected_result)

    def test_all_paths_between_with_n_stops(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        another_town = 'D'
        default_distance = 10
        self.graph.add_edge(first_town, second_town, default_distance)
        self.graph.add_edge(second_town, third_town, default_distance)
        self.graph.add_edge(third_town, another_town, default_distance)
        self.graph.add_edge(third_town, first_town, default_distance)
        result = self.graph.all_paths_between_with_n_stops(first_town, another_town, 6)
        expected_result = [[first_town, second_town, third_town, first_town, second_town, third_town,another_town]]
        self.assertEqual(result, expected_result)

    def test_all_paths_between_method_with_invalid_arguments(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        another_town = 'D'
        default_distance = 10
        self.graph.add_edge(first_town, second_town, default_distance)
        self.graph.add_edge(second_town, third_town, default_distance)
        self.graph.add_edge(third_town, another_town, default_distance)
        self.graph.add_edge(third_town, first_town, default_distance)
        self.assertRaises(ValueError, self.graph.all_paths_between, first_town, another_town, invalid_argument='invalid')

    def test_all_paths_between_method_for_same_town(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        default_distance = 10
        self.graph.add_edge(first_town, second_town, default_distance)
        self.graph.add_edge(first_town, third_town, default_distance)
        self.graph.add_edge(second_town, first_town, default_distance)
        self.graph.add_edge(third_town, first_town, default_distance)
        first_path = [first_town, second_town, first_town]
        second_path =[first_town, third_town, first_town] 
        result = self.graph.all_paths_between(first_town, first_town)
        expected_result = [first_path, second_path]
        self.assertEqual(sorted(result), sorted(expected_result))

    def test_all_paths_between_method_with_max_stops_passing_for_target_town(self):
        first_town = 'A'
        second_town = 'B'
        default_distance = 10
        self.graph.add_edge(first_town, second_town, default_distance)
        self.graph.add_edge(second_town, first_town, default_distance)
        first_path = [first_town, second_town]
        second_path =[first_town, second_town, first_town, second_town]
        result = self.graph.all_paths_between(first_town, second_town, max_stops=3)
        expected_result = [first_path, second_path]
        self.assertEqual(sorted(result), sorted(expected_result))

    def test_shortest_path_distance_between_method(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        another_town = 'D'
        default_distance = 10
        self.graph.add_edge(first_town, second_town, default_distance)
        self.graph.add_edge(second_town, third_town, default_distance)
        self.graph.add_edge(third_town, another_town, default_distance)
        self.graph.add_edge(second_town, another_town, default_distance)
        result = self.graph.shortest_distance_path_between(first_town, another_town)
        shortest_path = [first_town, second_town, another_town]
        self.assertEqual(result, shortest_path)

    def test_shortest_path_time_between_method(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        default_time = 1
        default_distance = 10
        self.graph.add_edge(first_town, second_town, default_distance, default_time)
        self.graph.add_edge(second_town, third_town, default_distance, default_time)
        self.graph.add_edge(first_town, third_town, 30, default_time)
        result = self.graph.shortest_time_path_between(first_town, third_town)
        shortest_time = [first_town, third_town]
        self.assertEqual(result, shortest_time)

    def test_shortest_path_distance_between_method_without_path_between_towns(self):
        first_town = 'A'
        second_town = 'B'
        default_distance = 10
        self.graph.create_node(first_town)
        self.graph.create_node(second_town)
        result = self.graph.shortest_distance_path_between(first_town, second_town)
        shortest_path = []
        self.assertEqual(result, shortest_path)

    def test_shortest_distante_between_method_without_path_beween_towns(self):
        first_town = 'A'
        second_town = 'B'
        default_distance = 10
        self.graph.create_node(first_town)
        self.graph.create_node(second_town)
        result = self.graph.shortest_distance_between(first_town, second_town)
        shortest_path = None
        self.assertEqual(result, shortest_path)

    def test_shortest_distante_between_method_without_path_beween_towns(self):
        first_town = 'A'
        second_town = 'B'
        third_town = 'C'
        default_distance = 10
        self.graph.create_node(first_town)
        self.graph.create_node(second_town)
        self.graph.add_edge(first_town, second_town, default_distance)
        self.graph.add_edge(second_town, third_town, default_distance)
        self.graph.add_edge(first_town, third_town, default_distance * 3)
        result = self.graph.shortest_distance_between(first_town, third_town)
        shortest_path = default_distance * 2
        self.assertEqual(result, shortest_path)


if __name__ == '__main__':
    unittest.main()
