import unittest
import context
from trains.town import Town

class TestTownMethods(unittest.TestCase):
    def setUp(self):
        self.town = Town('T')

    def tearDown(self):
        self.town = None

    def test_town_with_a_invalid_neighboring_towns(self):
        expected_result = {}
        result = self.town.neighboring_towns
        self.assertEqual(result, expected_result)

    def test_add_neighboring_town_method(self):
        another_town = Town('A')
        distance_to_another_town = 10
        time_to_another_town = 1
        self.town.add_neighboring_town(another_town, distance_to_another_town, time_to_another_town)
        expected_result = {'distance':distance_to_another_town, 'time': time_to_another_town}
        result = self.town.neighboring_towns[another_town]
        self.assertEqual(result, expected_result)

if __name__ == '__main__':
    unittest.main()
